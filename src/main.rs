use anyhow::Result;
use binrw::BinWrite;
use camino::Utf8PathBuf;
use cargo_metadata::{Artifact, Message};
use clap::Clap;
use pebble_bin::{json::*, pbpack::ResourcePack, stm32_crc, PebbleBinary};
use std::collections::HashMap;
use std::env::var_os;
use std::ffi::OsString;
use std::fs::{self, File};
use std::io::{BufReader, Cursor, Write};
use std::process::{Command, Stdio};
use zip::write::{FileOptions, ZipWriter};

#[derive(Clap)]
struct Opts {
    #[clap(short, long)]
    pub install: bool,

    #[clap(short, long)]
    pub logs: bool,

    pub args: Vec<OsString>,
}

fn make_pbw(executable_path: Utf8PathBuf) -> Result<Utf8PathBuf> {
    let opts = FileOptions::default();

    // TODO: resources
    let resources = ResourcePack {
        num_files: 0,
        crc: 0xFFFFFFFF,
        timestamp: 0,
        table_entries: Vec::new(),
    };

    let out_path = executable_path.with_extension("pbw");
    let out_file = File::create(&out_path)?;
    let mut pbw = ZipWriter::new(out_file);

    pbw.start_file("appinfo.json", opts)?;
    let name = executable_path.file_name().unwrap_or("<missing>");
    let mut dummy_map = HashMap::new();
    dummy_map.insert("dummy".to_string(), 10000);
    let appinfo = AppInfo {
        target_platforms: vec!["basalt".to_string()],
        display_name: name.to_string(),
        name: name.to_string(),
        message_keys: dummy_map.clone(),
        company_name: name.to_string(),
        capabilities: vec![],
        sdk_version: "3".to_string(),
        enable_multi_js: true,
        version_label: "1.0".to_string(),
        app_keys: dummy_map,
        long_name: name.to_string(),
        short_name: name.to_string(),
        watchapp: WatchappInfo { watchface: false },
        resources: Resources {
            media: Default::default(),
        },
        uuid: "00010203-0405-0607-0809-0a0b0c0d0e0f".to_string(),
    };
    serde_json::to_writer_pretty(&mut pbw, &appinfo)?;

    pbw.add_directory("basalt", opts)?;

    pbw.start_file("basalt/app_resources.pbpack", opts)?;
    let mut resource_data = Vec::new();
    resources.write_to(&mut Cursor::new(&mut resource_data))?;
    pbw.write_all(&resource_data)?;

    pbw.start_file("basalt/pebble-app.bin", opts)?;
    let elf = fs::read(executable_path)?;
    let binary = PebbleBinary::new(&elf)?;
    let mut binary_data = Vec::new();
    binary.write(&mut Cursor::new(&mut binary_data))?;
    pbw.write_all(&binary_data)?;

    pbw.start_file("basalt/manifest.json", opts)?;
    let manifest = BinaryManifest {
        manifest_version: 2,
        generated_by: "cargo-pbw".to_string(),
        generated_at: 0,
        application: BinaryInfo {
            sdk_version: Version {
                major: 5,
                minor: 86,
            },
            file: FileInfo {
                timestamp: 0,
                crc: stm32_crc::crc32(&binary_data),
                name: "pebble-app.bin".to_string(),
                size: binary_data.len(),
            },
        },
        debug: Default::default(),
        r#type: "application".to_string(),
        resources: FileInfo {
            timestamp: 0,
            crc: stm32_crc::crc32(&resource_data),
            name: "app_resources.pbpack".to_string(),
            size: resource_data.len(),
        },
    };
    serde_json::to_writer(&mut pbw, &manifest)?;

    pbw.finish()?;

    Ok(out_path)
}

fn main() -> Result<()> {
    let mut cargo = if let Some(cargo_var) = var_os("CARGO") {
        Command::new(cargo_var)
    } else {
        Command::new("cargo")
    };

    let args = if std::env::args().nth(1) == Some("pbw".to_string()) {
        std::env::args_os().skip(1)
    } else {
        std::env::args_os().skip(0)
    };

    let opts = Opts::parse_from(args);

    let mut command = cargo
        .args(&["build", "--message-format=json-diagnostic-rendered-ansi"])
        .args(&opts.args)
        .stdout(Stdio::piped())
        .spawn()?;

    let stdout = BufReader::new(command.stdout.take().unwrap());

    let mut compiler_messages = vec![];

    let mut pbw = None;

    let messages = Message::parse_stream(stdout);
    for message in messages {
        match message? {
            Message::CompilerArtifact(Artifact {
                executable: Some(executable),
                ..
            }) => {
                pbw = Some(make_pbw(executable)?);
            }
            Message::CompilerMessage(msg) => {
                compiler_messages.push(msg);
            }
            _ => {}
        }
    }

    if let Some(exit_code) = command.wait()?.code() {
        for msg in compiler_messages {
            if let Some(msg) = msg.message.rendered {
                println!("{}", msg);
            } else {
                println!("{:?}", msg);
            }
        }

        if exit_code == 0 && (opts.install || opts.logs) {
            if let Some(pbw) = pbw {
                let mut command = Command::new("pebble");

                command.arg("install");
                if opts.logs {
                    command.arg("--logs");
                }
                command.arg(pbw);

                let status = command.status()?;

                if let Some(exit_code) = status.code() {
                    std::process::exit(exit_code);
                } else {
                    std::process::exit(1);
                }
            }
        }

        std::process::exit(exit_code);
    } else {
        std::process::exit(1);
    }
}
